extern crate journal;

use std::fs::{File, OpenOptions};
use std::io::{Read, Write, Seek, SeekFrom};
use std::ops::{Deref, DerefMut};
use journal::Journal;

fn main() {
    let mut f = File::open("foo.bin").unwrap();
    let mut but = Vec::with_capacity(f.metadata().unwrap().len() as usize);
    let mut buf = Vec::with_capacity(f.metadata().unwrap().len() as usize);

    f.read_to_end(&mut but).unwrap();
    f.seek(SeekFrom::Start(0));
    f.read_to_end(&mut buf).unwrap();
    assert_eq!(&buf,&but);
    let s = Journal::parse(but);
    //parsing works
    let s2 = s.ser();
    assert_eq!(buf,s2);
    //write header
}
