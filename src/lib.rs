#![feature(int_to_from_bytes)]
//TODO:add corrupt journal recovery! Make it so the parser doesn't crash if it sees incorrect data.
//This means that if a first entry is valid, but the second one isn't, execute the first one and
//throw out everything after the second one, including the second one.  use std::io; use std::io::prelude::*; use std::default::Default;
extern crate hex;

use std::fs::{File, OpenOptions};
use std::io::{Read, Write, Seek, SeekFrom};
use std::ops::{Deref, DerefMut};
use hex::encode;
use std::default;



#[derive(Default,Clone,Debug)]
struct State {
    jlen: u64,
    loc: u64,
    typ: u8,
    len: u64,
    dat: Vec<u8>,
    past: Vec<JournalEntry>
}

#[derive(Default,Clone,Debug,PartialEq)]
struct JournalEntry {
    loc: u64,
    typ: u8,
    dat: Vec<u8>
}
//builder pattern!
//
impl JournalEntry {
    fn ser(&self) -> Vec<u8> {
        let mut buf = vec!();
        let l = self.loc.to_le_bytes();
        buf.extend_from_slice(&l);
        buf.push(self.typ);
        let mut dat = self.dat.clone();
        buf.extend_from_slice(&dat.len().to_le_bytes());
        //fix me to extend from slice
        buf.append(&mut dat);
        

        
        buf
    }
}
#[derive(Default,Clone,Debug,PartialEq)]
pub struct Journal {
    je: Vec<JournalEntry>,
}

impl Journal {
    pub fn parse(v: Vec<u8>) -> Journal { 
        Journal {
            je: gnb(&v).past,
        }
    }
    pub fn new() -> Journal {
        Default::default()
    }
    pub fn with(self, loc: u64, typ: u8, dat: Vec<u8>) -> Self {
        let mut n = self.clone();
        n.je.push(JournalEntry {
            loc: loc,
            typ: typ,
            dat: dat,
        });
        n
    }
    pub fn ser(&self) -> Vec<u8> {
        println!("SERIAL");
        let mut buf = vec!();
        let mut i = 0;
        println!("LEN:{}",&self.je.len());
        for entry in &self.je {
            //fixme to not use append
            let mut s = entry.ser();
            buf.append(&mut s);
            if &self.je.len()-1 != i {
                buf.push(0x45);
                buf.push(0x45);
            }
            i+=1;

        }
        let mut fbuf = vec!();
        let buflen = buf.len().to_le_bytes();

        //the +1 is for the 0x45 at the beginning, the buflen.len() is for the length of the length
        //of the buffer, and buf.len is for the length of the buffer
        //This explanation needs help
        let jlen = buflen.len()+buf.len()+1;
        fbuf.push(0x45);
        fbuf.extend_from_slice(&(buflen.len()+buf.len()+1).to_le_bytes());
        fbuf.append(&mut buf);
        
        fbuf
    }
}
fn gnb(stream: &[u8]) -> State {
    //don't convert that number to decimal
    if stream[0] == 0x45u8 {
        return getjlen(&stream[1..])
    } else {
        panic!();
    }
}
//this is the toal journal length, INCLUDING LITERALLY EVERYTHING
fn getjlen(stream: &[u8]) -> State{
    //10 
    let mut s = State::default();
    let mut col = [0;8];
    
    let mut i = 0;
    for b in stream[0..8].iter() {
        col[i]=*b;
        i+=1;
    }
//    println!("gytes: {:?}", col);
    let jlen = u64::from_le_bytes(col);
    println!("jlen: {}",jlen);
     
    s.jlen=jlen;

    getloc(&stream[8..],s)

}
fn getloc(stream: &[u8], s: State) -> State{
    //10
    let mut s = s.clone();
    let mut col = [0;8];
    
    let mut i = 0;
    for b in stream[0..8].iter() {
        col[i]=*b;
        i+=1;
    }
    let thingy = u64::from_le_bytes(col);
    println!("loc: {}",thingy);
    s.loc=thingy;
    gettype(&stream[8..],s)

}
fn gettype(stream: &[u8], s: State) -> State {
    let mut s = s.clone();
    let ty = stream[0];
    s.typ = ty;
    println!("ty:{}", ty);
    return getlen(&stream[1..], s);
}
fn getlen(stream: &[u8], s: State) -> State{
    let mut s = s.clone();
    let mut col = [0;8];
    
    let mut i = 0;
    for b in stream[0..8].iter() {
        col[i]=*b;
        i+=1;
    }
    let thingy = u64::from_le_bytes(col);
    println!("len: {}",thingy);
    s.len=thingy;
    getdat(&stream[8..],s)
}
fn getdat(stream: &[u8], s: State) -> State{
    println!("DAT");
    //make me cleaner
    let mut s = s.clone();

    let mut i = 0;
    let mut buf = vec!();
    while i != s.len {
        println!("{}:{}",i,stream[i as usize]);
        buf.push(stream[i as usize]);
        i += 1;
    }
    s.dat = buf;

    return get45(&stream[s.len as usize..],s);
}

fn get45(stream: &[u8], s: State) -> State {
    let mut s = s.clone();
    if stream.len() > 2 {
        if stream[0..2] == [0x45,0x45] {
            println!("chzeck");
        } else {
            //if there is more data, but there is not the proper terminator of the previous data,
            //panic
            //fix this implementation to throw out the rest and jump outside of the if stream.len
            //case
            println!("{:?}",&stream[0..2]);
            panic!();
        }
        s.past.push(JournalEntry {
            loc: s.loc,
            typ: s.typ,
            dat: s.dat.clone(),
        });
        return getloc(&stream[2..],s)
    }
    //if there are no more journal entries to parse, put the previous one in and be done.
    s.past.push(JournalEntry {
        loc: s.loc,
        typ:s.typ,
        dat:s.dat.clone(),
    });
    s
}

/*
fn main() {
    let jour = Journal::new().with(1234, 0x54, vec![1,2,3,4,5]).with(1234, 0x54, vec![1,2,3,4,5]);
    let mut f = OpenOptions::new().read(true).write(true).open("foo.db").unwrap();
    f.seek(SeekFrom::Start(0));
    let enc = jour.ser();
    println!("{}",encode(&enc));
    f.write_all(&enc).unwrap();
}*/

